import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    equipo:[]
  },
  mutations: {
  },
  actions: {
    async eliminarE({commit}, objeto){
      const peticion = await fetch('http://localhost:3000/equipo', {
        method: 'DELETE',
        headers:{
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(objeto)
      });

    }

  },
  modules: {
  }
})
